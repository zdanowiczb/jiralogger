# python jiralogger.py  -l username -p pwd -s https://sever.atlassian.net/

from jira import JIRA
import argparse
import sys
import pytz
import datetime

parser = argparse.ArgumentParser()
parser.add_argument('-l', '--login', action='store', required=False, help='User name')
parser.add_argument('-p', '--password', action='store', required=False, help='User password')
parser.add_argument('-s', '--server', action='store', required=False, help='Sever address', default = 'https://jira.atlassian.com')
results = parser.parse_args()

def isWeekday(day):
    return day.weekday() != 5 and day.weekday() != 6
 
def findAllDaysInRange(start, end, weekends):
    daysRange = []
    days = (end - start).days
    for day in range(0, days + 1):
        daysRange.append(start + datetime.timedelta(days=day))
    if(weekends == 'false'):
        return [elem for elem in daysRange if isWeekday(elem)]
    else:
        return daysRange
    
def logIn(login, password, server):
    jiraOptions = {
        'server': server
    }
    return JIRA(options=jiraOptions, basic_auth=(login, password))

    
def logWorkMenu(jiraObject):
    logInfo = input('Provide information separated by semicolon\n' +
    'issueName - code name of the issue (eg. TASK-1)\n' + 
    'timePerDay - time that you want to report for each day (eg. 8h)\n'
    'startDay - day from you want to start logging (YY-MM-DD-HH-MM format)\n'
    'workDestription - description of work that was made\n'
    'stopDay - day from you want to stop logging (YY-MM-DD-HH-MM format)\n'
    'weekends - log time on weekends (true/false = default)\n'
    'Example: TASK-1;7.5h;2015-12-1-8-0;Fixing production code;2015-12-6-8-0;true\n')
    logInfo = logInfo.split(';')
    try:
        issue = jira.issue(logInfo[0]);
        timePerDay = logInfo[1] if 1 < len(logInfo) else None
        startDay = logInfo[2] if 2 < len(logInfo) else None
        workDescription = logInfo[3] if 3 < len(logInfo) else None
        stopDay = logInfo[4] if 4 < len(logInfo) else None
        weekends = logInfo[5] if 5 < len(logInfo) else None
        
        if(startDay != None):
            startYear, startMonth, startDay, startHour, startMinute = map(int, startDay.split('-'))
            startDate = datetime.datetime(startYear, startMonth, startDay, startHour, startMinute, 0, 0, tzinfo=pytz.UTC)
        else:
            startDate = datetime.datetime.now(tzinfo=pytz.UTC)
        if(stopDay != None):
            stopYear, stopMonth, stopDay, stopHour, stopMinute = map(int, stopDay.split('-'))
            stopDate = datetime.datetime(stopYear, stopMonth, stopDay, stopHour, stopMinute, 0, 0, tzinfo=pytz.UTC)
        else:
            stopDate = startDate
        try:
            days = findAllDaysInRange(startDate, stopDate, weekends)
            
            print('Do you want report time for all following days?')
            for day in days:
                print(day)
                print("Time spent: " + timePerDay)
            if(input('yes/no\n') == 'yes'):
                for day in days:
                    jira.add_worklog(issue, timeSpent = timePerDay, started = day, comment = workDescription)
                print("Finished logging.")
        except:
            print('Error occured during time logging.')
            
    except:
        print('Issue ' + logInfo[0] + ' was not found')

if(results.login and results.password):
    try:
        logged = False
        jira = logIn(results.login, results.password, results.server)
        logged = True
    except:
        print('Cannot log in. Check if provided credentials are valid')

if(logged):
    print('You are logged as ' + jira.current_user())

ans = True
while ans:
    print ("""
    1.Log work
    9.Exit
    """)
    ans=input('What would you like to do, ' + jira.current_user() + '?\n') 
    if ans=="1": 
      logWorkMenu(jira) 
    elif ans=="9":
      print("Goodbye")
      sys.exit()
    elif ans !="":
      print("Not valid choice. Try again\n") 
